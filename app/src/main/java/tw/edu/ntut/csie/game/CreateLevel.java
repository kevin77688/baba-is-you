package tw.edu.ntut.csie.game;

import android.content.res.Resources;

import java.util.ArrayList;

import tw.edu.ntut.csie.game.core.MovingBitmap;
import tw.edu.ntut.csie.game.pattern.builder.BuildObject;
import tw.edu.ntut.csie.game.pattern.singleton.GlobalVariable;

public class CreateLevel {

    private BuildObject _builder;
    private MovingBitmap _background;

    public CreateLevel(int level) {
        _builder = new BuildObject();
        Resources res = GlobalVariable.getInstance().context.getResources();
        String[] blockInfo = res.getStringArray(Dictionary.getLevel(level));
        for (String s : blockInfo){
            String[] splitedString = s.split(",");
            if (splitedString.length != 3)
                throw new RuntimeException("Input XML error !\n Input is : " + s);
            _builder.buildObject(splitedString[0], Integer.parseInt(splitedString[1]), Integer.parseInt(splitedString[2]));
        }
        _background = new MovingBitmap(R.drawable.default_background);
        if (level == -1)
            _background = new MovingBitmap(R.drawable.easter1_background);
    }

    public Location get_boundary_location() {
        return _builder.getBoundaryLocation();
    }

    public ArrayList<Location> get_locations() {
        return _builder.getBlockLocation();
    }

    public ArrayList<MovableObject> get_blocks() {
        return _builder.getBlockList();
    }

    public MovingBitmap getBackground() {
        return _background;
    }

}
