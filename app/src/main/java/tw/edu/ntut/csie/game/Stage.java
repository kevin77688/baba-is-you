package tw.edu.ntut.csie.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import tw.edu.ntut.csie.game.core.Audio;
import tw.edu.ntut.csie.game.core.MovingBitmap;
import tw.edu.ntut.csie.game.extend.BitmapButton;
import tw.edu.ntut.csie.game.extend.ButtonEventHandler;
import tw.edu.ntut.csie.game.gameFrameworkCore.Game;
import tw.edu.ntut.csie.game.gameFrameworkCore.Pointer;
import tw.edu.ntut.csie.game.pattern.command.CommandManager;
import tw.edu.ntut.csie.game.pattern.command.MoveCommand;
import tw.edu.ntut.csie.game.pattern.observer.StaticObserver;
import tw.edu.ntut.csie.game.pattern.singleton.GlobalVariable;

public class Stage {
    // Screen size 600 * 350
    private ArrayList<MovableObject> _blockList;
    private ArrayList<MovableObject> _currentMovingObject;
    private CommandManager _commandManager;
    private GameMap _map;
    private LogicalBlock _logicalBlock;
    private int _level;
    private MovingBitmap _background, _pause_background;
    private boolean _bonus, _is_pause;
    private Audio _bird_up, _let_me_in;
    private BitmapButton _resume_button, _map_button, _menu_button, _restart_button, _easter_button;
    private ArrayList<BitmapButton> _allButton;

    public Stage(Map<String, Object> data) {
        _level = (int) data.get("level");
        _bonus = _level < 0;
        _is_pause = false;
        if (_level == -1) {
            _bird_up = new Audio(R.raw.bird_up);
            _let_me_in = new Audio(R.raw.let_me_in);
        }
        _allButton = new ArrayList<>();
        CreateLevel level_n = new CreateLevel(_level);
        _commandManager = new CommandManager();
        _blockList = level_n.get_blocks();
        _map = new GameMap(level_n.get_boundary_location(), level_n.get_blocks(), level_n.get_locations());
        _background = level_n.getBackground();
        _pause_background = new MovingBitmap(R.drawable.pause_background);
        _logicalBlock = new LogicalBlock(_blockList, _map);
        _currentMovingObject = _logicalBlock.getCurrentMovingObject();
        initializeButton();

    }

    private void initializeButton() {
        _resume_button = new BitmapButton(R.drawable.resume, R.drawable.resume_pressed, 100, 50);
        _map_button = new BitmapButton(R.drawable.map, R.drawable.map_pressed, 100, 100);
        _restart_button = new BitmapButton(R.drawable.restart, R.drawable.restart_pressed, 100, 150);
        _menu_button = new BitmapButton(R.drawable.pause_menu, R.drawable.pause_menu_pressed, 100, 200);
        _easter_button = new BitmapButton(R.drawable.easter_button, R.drawable.easter_button, 450, 200);
        _resume_button.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                _is_pause = false;
            }
        });
        _map_button.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                StaticObserver.getInstance().setChangeState(true);
            }
        });
        _restart_button.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                StaticObserver.getInstance().setRestart();
            }
        });
        _menu_button.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                StaticObserver.getInstance().setBackToMenu();
            }
        });
        _easter_button.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                StaticObserver.getInstance().setEaster();
            }
        });
        _allButton.add(_resume_button);
        _allButton.add(_map_button);
        _allButton.add(_menu_button);
        _allButton.add(_restart_button);
        _allButton.add(_easter_button);

        for (BitmapButton button : _allButton)
            button.setVisible(false);
    }

    public void up() {
        checkCommandExecutable("push", 0);
        if (_bonus)
            _bird_up.play();
    }

    public void down() {
        checkCommandExecutable("push", 1);
    }

    public void left() {
        checkCommandExecutable("push", 2);
    }

    public void right() {
        checkCommandExecutable("push", 3);
    }

    public void undo() {
        _logicalBlock.undoLogical();
        _commandManager.Undo();
    }

    public void redo() {
        _commandManager.Redo();
        _logicalBlock.redoLogical();
    }

    public void setLogical() {
        _logicalBlock.setLogical(_map.getBlockChanges());
    }

    private void checkCommandExecutable(String status, int direction) {
        Location _bird_before = null;
        MovableObject bird = null;

        if (!_currentMovingObject.isEmpty()) {
            if (_bonus) {
                bird = _currentMovingObject.get(0);
                _bird_before = _map.getMovingObjectRelativeLocation(bird);
            }
            for(MovableObject checkObject : _currentMovingObject)
                if (checkObject.isDefeat())
                    return;
            ArrayList<Stack<MovableObject>> checkArray = _map.checkMovedBlock(_currentMovingObject, status, direction);
            ArrayList<Stack<MovableObject>> executeArray = new ArrayList<>();
            for (Stack<MovableObject> stack : checkArray)
                if (!stack.isEmpty())
                    executeArray.add(stack);
            if (!executeArray.isEmpty())
                _commandManager.Execute(new MoveCommand(_map, executeArray, direction));
            setLogical();
            if (_bonus)
                if (_map.getMovingObjectRelativeLocation(bird).equals(_bird_before)) {
                    _bird_up.stop();
                    _let_me_in.play();
                }
        }
        GlobalVariable.getInstance().effects.play(Game.MOVING_SOUND);
    }

    public int getLevel(){
        return _level;
    }

    private void moveObjectToFront() {
        ArrayList<MovableObject> delayObject = new ArrayList<>();
        ArrayList<MovableObject> newBlockList = new ArrayList<>();
        for (MovableObject object : _blockList)
            if (!object.isYou() && !object.isPush())
                newBlockList.add(object);
            else
                delayObject.add(object);
        newBlockList.addAll(delayObject);
        _blockList = newBlockList;
    }

    public void pause(){
        _is_pause = true;
    }

    public void move() {
        for (MovableObject movableObject : _blockList)
            movableObject.move();
        _currentMovingObject = _logicalBlock.getCurrentMovingObject();
        moveObjectToFront();
    }

    public void show() {
        _background.show();
        for (MovableObject movableObject : _blockList)
            movableObject.show();
    }

    public void show_pause() {
        if (_is_pause) {
            _pause_background.show();
            for (BitmapButton button : _allButton)
                button.setVisible(true);
        } else
            for (BitmapButton button : _allButton)
                button.setVisible(false);
        for (BitmapButton button : _allButton)
            button.show();
    }

    public void release() {
//        for(MovableObject object : _blockList)
//            object.release();
//        _blockList = null;
//        for (MovableObject object : _currentMovingObject)
//            object.release();
//        _currentMovingObject = null;
    }

    public boolean pointerPressed(Pointer actionPointer, List<Pointer> pointers) {
        for (BitmapButton button : _allButton)
            button.pointerPressed(actionPointer, pointers);
        return true;
    }

    public boolean pointerMoved(Pointer actionPointer, List<Pointer> pointers) {
        for (BitmapButton button : _allButton)
            button.pointerMoved(actionPointer, pointers);
        return false;
    }

    public boolean pointerReleased(Pointer actionPointer, List<Pointer> pointers) {
        for (BitmapButton button : _allButton)
            if (button != null)
                button.pointerReleased(actionPointer, pointers);
        return false;
    }
}