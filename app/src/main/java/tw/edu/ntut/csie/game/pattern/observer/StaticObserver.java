package tw.edu.ntut.csie.game.pattern.observer;

import java.util.Observable;

public class StaticObserver extends Observable {

    private static StaticObserver instance = new StaticObserver();
    // Singleton pattern & observer pattern (Static Observer)
    private boolean _isWin, _change_state, _backToMenu, _restart, _easter;

    private StaticObserver() {
        _isWin = false;
        _change_state = false;
        _backToMenu = false;
        _restart = false;
        _easter = false;
    }

    public static StaticObserver getInstance() {
        return instance;
    }

    public boolean getWinStatement() {
        return _isWin;
    }

    public boolean getChangeState() {
        return _change_state;
    }

    public boolean getBackToMenu() {
        return _backToMenu;
    }

    public boolean getRestart() {
        return _restart;
    }

    public boolean getEaster() {
        return _easter;
    }

    public void setWinStatement(boolean bool) {
        _isWin = bool;
        setChanged();
        notifyObservers();
        _isWin = false;
    }

    public void setChangeState(boolean bool) {
        _change_state = bool;
        setChanged();
        notifyObservers();
        _change_state = false;
    }

    public void setBackToMenu() {
        _backToMenu = true;
        setChanged();
        notifyObservers();
        _backToMenu = false;
    }

    public void setRestart() {
        _restart = true;
        setChanged();
        notifyObservers();
        _restart = false;
    }

    public void setEaster() {
        _easter = true;
        setChanged();
        notifyObservers();
        _easter = false;
    }
}
