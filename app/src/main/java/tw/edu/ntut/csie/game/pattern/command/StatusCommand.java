package tw.edu.ntut.csie.game.pattern.command;

import tw.edu.ntut.csie.game.MovableObject;

public class StatusCommand implements ICommand {

    MovableObject _currentObject;
    MovableObject _backupObject;
    String _status;

    public StatusCommand(MovableObject object, String status) {
        _currentObject = object;
        _backupObject = new MovableObject(object);
        _status = status;
        if (object.getNullTag())
            object.setAllStatusNull();
    }

    @Override
    public void Execute() {
        try{
            _currentObject.setStatus(_status);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void Discard() {
        _currentObject.recover(_backupObject);
    }
}
