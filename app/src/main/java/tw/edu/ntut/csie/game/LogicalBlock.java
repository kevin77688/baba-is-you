package tw.edu.ntut.csie.game;

import java.util.ArrayList;

import tw.edu.ntut.csie.game.pattern.command.AnimationCommand;
import tw.edu.ntut.csie.game.pattern.command.CommandManager;
import tw.edu.ntut.csie.game.pattern.command.ICommand;
import tw.edu.ntut.csie.game.pattern.command.MovableObjectStatusCommand;
import tw.edu.ntut.csie.game.pattern.command.StatusCommand;

public class LogicalBlock {
    private GameMap _map;
    private ArrayList<MovableObject> _blockList;
    private ArrayList<MovableObject> _blocksOfIs;
    private ArrayList<MovableObject> _blocksOfText;
    private CommandManager _commandManager;
    private ArrayList<ICommand> _allCommand;

    public LogicalBlock(ArrayList<MovableObject> blockList, GameMap map) {
        _map = map;
        _blockList = blockList;
        _blocksOfText = new ArrayList<>();
        _blocksOfText.addAll(getCertainTypeOfBlocks_contain("text"));
        _blocksOfIs = new ArrayList<>();
        _blocksOfIs.addAll(getCertainTypeOfBlocks_contain("text_is"));
        _allCommand = new ArrayList<>();
        _commandManager = new CommandManager();
        getLogical();
        for(ICommand command : _allCommand)
            command.Execute();
    }

    public ArrayList<MovableObject> getCurrentMovingObject() {
        ArrayList<MovableObject> resultCurrentMovingObject = new ArrayList<>();
        for (MovableObject object : _blockList)
            if (object.isYou())
                resultCurrentMovingObject.add(object);
        return resultCurrentMovingObject;
    }

    public ArrayList<MovableObject> getCertainTypeOfBlocks_contain(String string) {
        ArrayList<MovableObject> certainTypeOfBlocks = new ArrayList<>();
        for (MovableObject block : _blockList)
            if (block.getAnimation().contains(string))
                certainTypeOfBlocks.add(block);
        return certainTypeOfBlocks;
    }

    private void getLogical(){
        _allCommand.clear();
        for (MovableObject textObject : _blocksOfText)
            _allCommand.add(new StatusCommand(textObject, "push"));
        for (MovableObject object : _blocksOfIs) {
            ArrayList<BlockStatus> surroundingBlock = _map.getSurroundingBlocks(object);
            if (surroundingBlock.get(0).getSubject() != null)
                if (surroundingBlock.get(1).getObject() != null)
                    _allCommand.addAll(setStatus(surroundingBlock.get(0).getSubject(), surroundingBlock.get(1).getObject()));
                else if (surroundingBlock.get(1).getSubject() != null)
                    _allCommand.addAll(setAnimation(surroundingBlock.get(0).getSubject(), surroundingBlock.get(1).getSubject()));
            if (surroundingBlock.get(2).getSubject() != null)
                if (surroundingBlock.get(3).getObject() != null)
                    _allCommand.addAll(setStatus(surroundingBlock.get(2).getSubject(), surroundingBlock.get(3).getObject()));
                else if (surroundingBlock.get(3).getSubject() != null)
                    _allCommand.addAll(setAnimation(surroundingBlock.get(2).getSubject(), surroundingBlock.get(3).getSubject()));
        }
    }

    public void setLogical(ArrayList<ICommand> blockChanges) {
        for (MovableObject object : _blockList)
            if (object.isShow())
                object.setNullTag();
        getLogical();
        _allCommand.addAll(blockChanges);
        if (!_allCommand.isEmpty())
            _commandManager.Execute(new MovableObjectStatusCommand(_allCommand));
        for (MovableObject object : _blockList)
            if (object.getNullTag())
                object.setAllStatusNull();
    }

    public void undoLogical() {
        _commandManager.Undo();
    }

    public void redoLogical() {
        _commandManager.Redo();
    }

    private ArrayList<ICommand> setStatus(MovableObject subject, MovableObject object) {
        ArrayList<ICommand> resultCommand = new ArrayList<>();
        String subjectString = Dictionary.getString(subject.getAnimation());
        String objectString = Dictionary.getString(object.getAnimation());
        for (MovableObject movableObject : _blockList)
            if (movableObject.getAnimation().equals(subjectString))
                resultCommand.add(new StatusCommand(movableObject, objectString));
//        if (resultCommand.isEmpty())
//            throw new RuntimeException("MovableObject cannot found");
        return resultCommand;
    }

    private ArrayList<ICommand> setAnimation(MovableObject subject, MovableObject object) {
        ArrayList<ICommand> resultCommand = new ArrayList<>();
        String subjectString = Dictionary.getString(subject.getAnimation());
        String objectString = Dictionary.getString(object.getAnimation());
        for (MovableObject movableObject : _blockList)
            if (movableObject.getAnimation().equals(subjectString))
                resultCommand.add(new AnimationCommand(movableObject, objectString));
//        if (resultCommand.isEmpty())
//            throw new RuntimeException("MovableObject cannot found");
        return resultCommand;
    }
}
