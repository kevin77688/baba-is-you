package tw.edu.ntut.csie.game.pattern.command;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

import tw.edu.ntut.csie.game.GameMap;
import tw.edu.ntut.csie.game.MovableObject;

public class MoveCommand implements ICommand {

    private GameMap _map;
    private ArrayList<Stack<MovableObject>> _currentMovingObject;
    private int _direction;
    private ArrayList<ICommand> _arrayOfFacingCommand;

    public MoveCommand(GameMap map, ArrayList<Stack<MovableObject>> arrayOfStack, int direction) {

        _map = map;
        _currentMovingObject = copyArrayList(arrayOfStack);
        _direction = direction;
        _arrayOfFacingCommand = new ArrayList<>();
    }

    public void Execute() {
        setMoved();
        ArrayList<Stack<MovableObject>> copyArray = copyArrayList(_currentMovingObject);
        switch (_direction) {
            case 0:
                _map.up(copyArray);
                break;
            case 1:
                _map.down(copyArray);
                break;
            case 2:
                _map.left(copyArray);
                break;
            case 3:
                _map.right(copyArray);
                break;
            default:
                throw new RuntimeException("Direction identifier error !");
        }
        setAnimationFacing();
    }

    public void Discard() {
        setMoved();
        ArrayList<Stack<MovableObject>> copyArray = copyArrayList(_currentMovingObject);
        switch (_direction) {
            case 0:
                _map.down(copyArray);
                break;
            case 1:
                _map.up(copyArray);
                break;
            case 2:
                _map.right(copyArray);
                break;
            case 3:
                _map.left(copyArray);
                break;
            default:
                throw new RuntimeException("Direction identifier error !");
        }
        undoAnimationFacing();
    }

    private ArrayList<Stack<MovableObject>> copyArrayList(ArrayList<Stack<MovableObject>> array) {
        ArrayList<Stack<MovableObject>> copyArray = new ArrayList<>();
        for (Stack<MovableObject> stack : array) {
            Stack<MovableObject> copyStack = new Stack<>();
            copyStack.addAll(stack);
            copyArray.add(copyStack);
        }
        return copyArray;
    }

    private void setAnimationFacing() {
        ArrayList<Stack<MovableObject>> copyArray = copyArrayList(_currentMovingObject);
        Iterator<Stack<MovableObject>> it = copyArray.iterator();
        while (it.hasNext()) {
            Stack<MovableObject> currentStack = it.next();
            while (!currentStack.isEmpty())
                _arrayOfFacingCommand.add(new AnimationFacingCommand(currentStack.pop(), _direction));
        }
        for (ICommand command : _arrayOfFacingCommand)
            command.Execute();
    }

    private void undoAnimationFacing() {
        for (ICommand command : _arrayOfFacingCommand)
            command.Discard();
    }

    private void setMoved() {
        for (Stack<MovableObject> stack : _currentMovingObject)
            for (MovableObject object : stack)
                object.setMoved();
    }
}
