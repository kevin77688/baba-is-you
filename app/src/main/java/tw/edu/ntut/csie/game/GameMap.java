package tw.edu.ntut.csie.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

import tw.edu.ntut.csie.game.pattern.command.ICommand;
import tw.edu.ntut.csie.game.pattern.command.StatusCommand;

public class GameMap {

    Location[][] _realMap;
    private ArrayList<MovableObject> _allObject;
    private Location _boundaryLocation;
    private BlockStatus[][] _relativeMovableObject;
    private ArrayList<StatusCommand> _blockChanges;

    public GameMap(Location boundaryLocation, ArrayList<MovableObject> objects, ArrayList<Location> objectsLocation) {
        _boundaryLocation = boundaryLocation;
        InitialAbsoluteMap();
        _allObject = new ArrayList<>();
        Iterator MovableObjectIterator = objects.iterator();
        Iterator LocationIterator = objectsLocation.iterator();
        while (MovableObjectIterator.hasNext())
            this.add((MovableObject) MovableObjectIterator.next(), (Location) LocationIterator.next());
        _blockChanges = new ArrayList<>();
    }

    private void InitialAbsoluteMap() {
        int unit = 350 / _boundaryLocation.getY();
        _realMap = new Location[_boundaryLocation.getX() + 1][_boundaryLocation.getY() + 1];
        _relativeMovableObject = new BlockStatus[_boundaryLocation.getX() + 1][_boundaryLocation.getY() + 1];
        for (int i = 0; i <= _boundaryLocation.getX(); i++)
            for (int j = 0; j <= _boundaryLocation.getY(); j++) {
                _realMap[i][j] = new Location((600 - 350) / 2 + 50 + unit * i, unit * j);
                _relativeMovableObject[i][j] = new BlockStatus();
            }
    }

    public void add(MovableObject object, Location relativeLocation) {
        _allObject.add(object);
        object.setLocation(_realMap[relativeLocation.getX()][relativeLocation.getY()]);
        _relativeMovableObject[relativeLocation.getX()][relativeLocation.getY()].add(object);
    }

    public ArrayList<Stack<MovableObject>> checkMovedBlock(ArrayList<MovableObject> movingObject, String status, int direction) {
        ArrayList<Stack<MovableObject>> checkArray = new ArrayList<>();
        for (MovableObject obj : movingObject) {
            Stack<MovableObject> checkList = new Stack<>();
            MovableObject currentCheck = obj;
            while (currentCheck != null) {
                checkList.push(currentCheck);
                Location relativeLocation = this.getMovingObjectRelativeLocation(currentCheck);
                currentCheck = null;
                boolean checkOutOfBound = true;
                switch (direction) {
                    case 0:
                        if (relativeLocation.getY() - 1 > 0) {
                            currentCheck = _relativeMovableObject[relativeLocation.getX()][relativeLocation.getY() - 1].checkStatus(status);
                            checkOutOfBound = false;
                        }
                        break;
                    case 1:
                        if (relativeLocation.getY() + 1 < _boundaryLocation.getY()) {
                            currentCheck = _relativeMovableObject[relativeLocation.getX()][relativeLocation.getY() + 1].checkStatus(status);
                            checkOutOfBound = false;
                        }
                        break;
                    case 2:
                        if (relativeLocation.getX() - 1 > 0) {
                            currentCheck = _relativeMovableObject[relativeLocation.getX() - 1][relativeLocation.getY()].checkStatus(status);
                            checkOutOfBound = false;
                        }
                        break;
                    case 3:
                        if (relativeLocation.getX() + 1 < _boundaryLocation.getX()) {
                            currentCheck = _relativeMovableObject[relativeLocation.getX() + 1][relativeLocation.getY()].checkStatus(status);
                            checkOutOfBound = false;
                        }
                        break;
                    default:
                        throw new RuntimeException("Direction identifier error !");
                }
                if ((currentCheck != null && currentCheck.isStop()) || checkOutOfBound) {
                    checkList.clear();
                    break;
                }
            }
            checkArray.add(checkList);
        }
        return checkArray;
    }

    public void up(ArrayList<Stack<MovableObject>> arrayOfStack) {
        Iterator<Stack<MovableObject>> it = arrayOfStack.iterator();
        while (it.hasNext()) {
            Stack<MovableObject> currentStack = it.next();
            while (!currentStack.isEmpty()) {
                MovableObject moveObject = currentStack.pop();
                if (moveObject.checkMoved()) {
                    Location currentLocation = getMovingObjectRelativeLocation(moveObject);
                    Location newLocation = new Location(currentLocation).goUp();
                    _blockChanges.addAll(_relativeMovableObject[newLocation.getX()][newLocation.getY()].add(moveObject));
                    _relativeMovableObject[currentLocation.getX()][currentLocation.getY()].remove(moveObject);
                    moveObject.setLocation(_realMap[newLocation.getX()][newLocation.getY()]);
                    moveObject.resetCheckMoved();
                }
            }
        }
    }

    public void down(ArrayList<Stack<MovableObject>> arrayOfStack) {
        Iterator<Stack<MovableObject>> it = arrayOfStack.iterator();
        while (it.hasNext()) {
            Stack<MovableObject> currentStack = it.next();
            while (!currentStack.isEmpty()) {
                MovableObject moveObject = currentStack.pop();
                if (moveObject.checkMoved()) {
                    Location currentLocation = getMovingObjectRelativeLocation(moveObject);
                    Location newLocation = new Location(currentLocation).goDown();
                    _blockChanges.addAll(_relativeMovableObject[newLocation.getX()][newLocation.getY()].add(moveObject));
                    _relativeMovableObject[currentLocation.getX()][currentLocation.getY()].remove(moveObject);
                    moveObject.setLocation(_realMap[newLocation.getX()][newLocation.getY()]);
                    moveObject.resetCheckMoved();
                }
            }
        }
    }

    public void left(ArrayList<Stack<MovableObject>> arrayOfStack) {
        Iterator<Stack<MovableObject>> it = arrayOfStack.iterator();
        while (it.hasNext()) {
            Stack<MovableObject> currentStack = it.next();
            while (!currentStack.isEmpty()) {
                MovableObject moveObject = currentStack.pop();
                if (moveObject.checkMoved()) {
                    Location currentLocation = getMovingObjectRelativeLocation(moveObject);
                    Location newLocation = new Location(currentLocation).goLeft();
                    _blockChanges.addAll(_relativeMovableObject[newLocation.getX()][newLocation.getY()].add(moveObject));
                    _relativeMovableObject[currentLocation.getX()][currentLocation.getY()].remove(moveObject);
                    moveObject.setLocation(_realMap[newLocation.getX()][newLocation.getY()]);
                    moveObject.resetCheckMoved();
                }
            }
        }
    }

    public void right(ArrayList<Stack<MovableObject>> arrayOfStack) {
        Iterator<Stack<MovableObject>> it = arrayOfStack.iterator();
        while (it.hasNext()) {
            Stack<MovableObject> currentStack = it.next();
            while (!currentStack.isEmpty()) {
                MovableObject moveObject = currentStack.pop();
                if (moveObject.checkMoved()) {
                    Location currentLocation = getMovingObjectRelativeLocation(moveObject);
                    Location newLocation = new Location(currentLocation).goRight();
                    _blockChanges.addAll(_relativeMovableObject[newLocation.getX()][newLocation.getY()].add(moveObject));
                    _relativeMovableObject[currentLocation.getX()][currentLocation.getY()].remove(moveObject);
                    moveObject.setLocation(_realMap[newLocation.getX()][newLocation.getY()]);
                    moveObject.resetCheckMoved();
                }
            }
        }
    }

    public Location getMovingObjectRelativeLocation(MovableObject object) {
        int x, y, maxX, maxY;
        Location returnLocation = null;
        maxX = _relativeMovableObject.length;
        maxY = _relativeMovableObject[0].length;
        for (int i = 0; i < maxX; i++)
            for (int j = 0; j < maxY; j++)
                if (_relativeMovableObject[i][j].checkObjectExist(object)) {
                    x = i;
                    y = j;
                    returnLocation = new Location(x, y);
                }
        if (returnLocation == null)
            throw new RuntimeException("Cannot find item");
        return returnLocation;
    }

    public ArrayList<BlockStatus> getSurroundingBlocks(MovableObject block) {
        ArrayList<BlockStatus> surroundingBlocks = new ArrayList<>();
        Location blockLocation = getMovingObjectRelativeLocation(block);
        if (blockLocation.getY() - 1 >= 0)
            surroundingBlocks.add(_relativeMovableObject[blockLocation.getX()][blockLocation.getY() - 1]);
        else
            surroundingBlocks.add(new BlockStatus());
        if (blockLocation.getY() + 1 < _relativeMovableObject.length)
            surroundingBlocks.add(_relativeMovableObject[blockLocation.getX()][blockLocation.getY() + 1]);
        else
            surroundingBlocks.add(new BlockStatus());
        if (blockLocation.getX() - 1 >= 0)
            surroundingBlocks.add(_relativeMovableObject[blockLocation.getX() - 1][blockLocation.getY()]);
        else
            surroundingBlocks.add(new BlockStatus());
        if (blockLocation.getX() + 1 < _relativeMovableObject[0].length)
            surroundingBlocks.add(_relativeMovableObject[blockLocation.getX() + 1][blockLocation.getY()]);
        else
            surroundingBlocks.add(new BlockStatus());
        return surroundingBlocks;
    }

    public ArrayList<ICommand> getBlockChanges() {
        ArrayList<ICommand> returnCommand = new ArrayList<>();
        returnCommand.addAll(_blockChanges);
        _blockChanges.clear();
        return returnCommand;
    }
}
