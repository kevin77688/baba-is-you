package tw.edu.ntut.csie.game;

public class Location {
    private int _x, _y;

    public Location(int x, int y) {
        this._x = x;
        this._y = y;
    }

    public Location(Location location) {
        this._x = location.getX();
        this._y = location.getY();
    }

    public int getX() {
        return _x;
    }

    public int getY() {
        return _y;
    }

    public Location goUp() {
        _y -= 1;
        return new Location(_x, _y);
    }

    public Location goDown() {
        _y += 1;
        return new Location(_x, _y);
    }

    public Location goLeft() {
        _x -= 1;
        return new Location(_x, _y);
    }

    public Location goRight() {
        _x += 1;
        return new Location(_x, _y);
    }

    public boolean equals(Location location) {
        return location.getX() == _x && location.getY() == _y;
    }
}
