package tw.edu.ntut.csie.game;

import java.util.Hashtable;

public class Dictionary {

    private static final Hashtable<String, Object> dictionaryWord = new Hashtable<>();

    static {
        // Subject block
        dictionaryWord.put("text_SUB_rock", "rock");
        dictionaryWord.put("text_SUB_wall", "wall_mid");
        dictionaryWord.put("text_SUB_baba", "baba");
        dictionaryWord.put("text_SUB_bird", "bird");
        dictionaryWord.put("text_SUB_flag", "flag");
        dictionaryWord.put("text_SUB_water", "water");
        dictionaryWord.put("text_SUB_skull", "skull");
        dictionaryWord.put("text_SUB_lava", "lava");

        // Object Block
        dictionaryWord.put("text_OBJ_push", "push");
        dictionaryWord.put("text_OBJ_stop", "stop");
        dictionaryWord.put("text_OBJ_you", "you");
        dictionaryWord.put("text_OBJ_win", "win");
        dictionaryWord.put("text_OBJ_sink", "sink");
        dictionaryWord.put("text_OBJ_defeat", "defeat");
        dictionaryWord.put("text_OBJ_hot", "hot");
        dictionaryWord.put("text_OBJ_melt", "melt");

        // Level Select
        dictionaryWord.put("level_1", R.array.level_1);
        dictionaryWord.put("level_2", R.array.level_2);
        dictionaryWord.put("level_3", R.array.level_3);
        dictionaryWord.put("level_4", R.array.level_4);
        dictionaryWord.put("level_5", R.array.level_5);
        dictionaryWord.put("level_6", R.array.level_6);

        // Easter
        dictionaryWord.put("level_-1", R.array.bird_up);
    }

    public static String getString(String inputString) {
        return (String) dictionaryWord.get(inputString);
    }

    public static int getLevel(int level) {
        String component = "level_" + Integer.toString(level);
        return (int) dictionaryWord.get(component);
    }

}
