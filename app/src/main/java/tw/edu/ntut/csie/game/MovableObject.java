package tw.edu.ntut.csie.game;

import java.util.ArrayList;

import tw.edu.ntut.csie.game.extend.Animation;

public class MovableObject extends Status {

    protected int _animationDirectionIndex = 3;
    protected ArrayList<Animation> _currentObjectAnimation;
    protected String _animationType;

    public MovableObject(String animationType) {
        _currentObjectAnimation = this.whichOne(animationType);
        _animationType = animationType;
    }

    public MovableObject(MovableObject object) {
        setAll(object);
    }

    public String getAnimation() {
        return _animationType;
    }

    public void setAnimation(String animationType) {
        _animationType = animationType;
        Location originalLocation = new Location(_currentObjectAnimation.get(_animationDirectionIndex).getLocation());
        _currentObjectAnimation = this.whichOne(_animationType);
        for (Animation animation : _currentObjectAnimation) {
            animation.setLocation(originalLocation);
        }
    }

    public void setAnimationFacing(int direction) {
        _animationDirectionIndex = direction;
    }

    public void setLocation(Location location) {
        for (Animation animation : _currentObjectAnimation)
            animation.setLocation(location.getX(), location.getY());
    }

    public void recover(MovableObject object) {
        setAll(object);
    }

    private void setAll(MovableObject object) {
        this._animationDirectionIndex = object._animationDirectionIndex;
        ArrayList<Animation> backupAnimationArray = new ArrayList<>();
        backupAnimationArray.addAll(object._currentObjectAnimation);
        this._currentObjectAnimation = backupAnimationArray;
        this._animationType = object._animationType;
        this.setAllStatus(object);
    }

    public void show() {
        if (_isShow)
            _currentObjectAnimation.get(_animationDirectionIndex).show();
    }

    public void move() {
        for (Animation animation : _currentObjectAnimation)
            animation.move();
    }

    public void release() {
        for (Animation animation : _currentObjectAnimation) {
            animation.release();
        }
    }

    // 決定block的動畫
    private ArrayList<Animation> whichOne(String i) {
        ArrayList<Animation> result = new ArrayList<>();
        Animation _animationUp, _animationDown, _animationLeft, _animationRight;
        _animationUp = new Animation();
        _animationDown = new Animation();
        _animationLeft = new Animation();
        _animationRight = new Animation();

        switch (i) {
            case "baba":
                _animationUp.addFrame(R.drawable.baba_up_1);
                _animationUp.addFrame(R.drawable.baba_up_2);
                _animationUp.addFrame(R.drawable.baba_up_3);
                _animationUp.addFrame(R.drawable.baba_up_4);
                _animationUp.addFrame(R.drawable.baba_up_5);
                _animationUp.setDelay(2);
                _animationDown.addFrame(R.drawable.baba_down_1);
                _animationDown.addFrame(R.drawable.baba_down_2);
                _animationDown.addFrame(R.drawable.baba_down_3);
                _animationDown.addFrame(R.drawable.baba_down_4);
                _animationDown.addFrame(R.drawable.baba_down_5);
                _animationDown.setDelay(2);
                _animationLeft.addFrame(R.drawable.baba_left_1);
                _animationLeft.addFrame(R.drawable.baba_left_2);
                _animationLeft.addFrame(R.drawable.baba_left_3);
                _animationLeft.addFrame(R.drawable.baba_left_4);
                _animationLeft.addFrame(R.drawable.baba_left_5);
                _animationLeft.setDelay(2);
                _animationRight.addFrame(R.drawable.baba_right_1);
                _animationRight.addFrame(R.drawable.baba_right_2);
                _animationRight.addFrame(R.drawable.baba_right_3);
                _animationRight.addFrame(R.drawable.baba_right_4);
                _animationRight.setDelay(2);
                break;
            case "bird":
                _animationUp.addFrame(R.drawable.rainbow_bird_up_1);
                _animationUp.addFrame(R.drawable.rainbow_bird_up_2);
                _animationUp.addFrame(R.drawable.rainbow_bird_up_3);
                _animationUp.addFrame(R.drawable.rainbow_bird_up_4);
                _animationUp.addFrame(R.drawable.rainbow_bird_up_5);
                _animationUp.addFrame(R.drawable.rainbow_bird_up_6);
                _animationUp.setDelay(2);
                _animationDown.addFrame(R.drawable.rainbow_bird_down_1);
                _animationDown.addFrame(R.drawable.rainbow_bird_down_2);
                _animationDown.addFrame(R.drawable.rainbow_bird_down_3);
                _animationDown.addFrame(R.drawable.rainbow_bird_down_4);
                _animationDown.addFrame(R.drawable.rainbow_bird_down_5);
                _animationDown.addFrame(R.drawable.rainbow_bird_down_6);
                _animationDown.setDelay(2);
                _animationLeft.addFrame(R.drawable.rainbow_bird_left_1);
                _animationLeft.addFrame(R.drawable.rainbow_bird_left_2);
                _animationLeft.addFrame(R.drawable.rainbow_bird_left_3);
                _animationLeft.addFrame(R.drawable.rainbow_bird_left_4);
                _animationLeft.addFrame(R.drawable.rainbow_bird_left_5);
                _animationLeft.addFrame(R.drawable.rainbow_bird_left_6);
                _animationLeft.setDelay(2);
                _animationRight.addFrame(R.drawable.rainbow_bird_right_1);
                _animationRight.addFrame(R.drawable.rainbow_bird_right_2);
                _animationRight.addFrame(R.drawable.rainbow_bird_right_3);
                _animationRight.addFrame(R.drawable.rainbow_bird_right_4);
                _animationRight.addFrame(R.drawable.rainbow_bird_right_5);
                _animationRight.addFrame(R.drawable.rainbow_bird_right_6);
                _animationRight.setDelay(2);
                break;
            case "rock":
                _animationUp.addFrame(R.drawable.rock_0_1);
                _animationUp.addFrame(R.drawable.rock_0_2);
                _animationUp.addFrame(R.drawable.rock_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "flag":
                _animationUp.addFrame(R.drawable.flag_0_1);
                _animationUp.addFrame(R.drawable.flag_0_2);
                _animationUp.addFrame(R.drawable.flag_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "water":
                _animationUp.addFrame(R.drawable.water_0_1);
                _animationUp.addFrame(R.drawable.water_0_2);
                _animationUp.addFrame(R.drawable.water_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "lava":
                _animationUp.addFrame(R.drawable.lava_1);
                _animationUp.addFrame(R.drawable.lava_2);
                _animationUp.addFrame(R.drawable.lava_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "wall_mid":
                _animationUp.addFrame(R.drawable.wall_5_1);
                _animationUp.addFrame(R.drawable.wall_5_2);
                _animationUp.addFrame(R.drawable.wall_5_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "skull":
                _animationUp.addFrame(R.drawable.skull_0_1);
                _animationUp.addFrame(R.drawable.skull_0_2);
                _animationUp.addFrame(R.drawable.skull_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_SUB_baba":
                _animationUp.addFrame(R.drawable.text_baba_0_1);
                _animationUp.addFrame(R.drawable.text_baba_0_2);
                _animationUp.addFrame(R.drawable.text_baba_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_SUB_bird":
                _animationUp.addFrame(R.drawable.text_bird_0_1);
                _animationUp.addFrame(R.drawable.text_bird_0_2);
                _animationUp.addFrame(R.drawable.text_bird_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_is":
                _animationUp.addFrame(R.drawable.text_is_0_1);
                _animationUp.addFrame(R.drawable.text_is_0_2);
                _animationUp.addFrame(R.drawable.text_is_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_OBJ_you":
                _animationUp.addFrame(R.drawable.text_you_0_1);
                _animationUp.addFrame(R.drawable.text_you_0_2);
                _animationUp.addFrame(R.drawable.text_you_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_SUB_flag":
                _animationUp.addFrame(R.drawable.text_flag_0_1);
                _animationUp.addFrame(R.drawable.text_flag_0_2);
                _animationUp.addFrame(R.drawable.text_flag_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_OBJ_win":
                _animationUp.addFrame(R.drawable.text_win_0_1);
                _animationUp.addFrame(R.drawable.text_win_0_2);
                _animationUp.addFrame(R.drawable.text_win_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_SUB_wall":
                _animationUp.addFrame(R.drawable.text_wall_0_1);
                _animationUp.addFrame(R.drawable.text_wall_0_2);
                _animationUp.addFrame(R.drawable.text_wall_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_SUB_skull":
                _animationUp.addFrame(R.drawable.text_skull_0_1);
                _animationUp.addFrame(R.drawable.text_skull_0_2);
                _animationUp.addFrame(R.drawable.text_skull_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_SUB_water":
                _animationUp.addFrame(R.drawable.text_water_0_1);
                _animationUp.addFrame(R.drawable.text_water_0_2);
                _animationUp.addFrame(R.drawable.text_water_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_SUB_lava":
                _animationUp.addFrame(R.drawable.text_lava_0_1);
                _animationUp.addFrame(R.drawable.text_lava_0_2);
                _animationUp.addFrame(R.drawable.text_lava_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_OBJ_stop":
                _animationUp.addFrame(R.drawable.text_stop_0_1);
                _animationUp.addFrame(R.drawable.text_stop_0_2);
                _animationUp.addFrame(R.drawable.text_stop_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_SUB_rock":
                _animationUp.addFrame(R.drawable.text_rock_0_1);
                _animationUp.addFrame(R.drawable.text_rock_0_2);
                _animationUp.addFrame(R.drawable.text_rock_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_OBJ_push":
                _animationUp.addFrame(R.drawable.text_push_0_1);
                _animationUp.addFrame(R.drawable.text_push_0_2);
                _animationUp.addFrame(R.drawable.text_push_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_OBJ_defeat":
//                R.drawable.text_baba_0_1;
                _animationUp.addFrame(R.drawable.text_defeat_0_1);
                _animationUp.addFrame(R.drawable.text_defeat_0_2);
                _animationUp.addFrame(R.drawable.text_defeat_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_OBJ_sink":
                _animationUp.addFrame(R.drawable.text_sink_0_1);
                _animationUp.addFrame(R.drawable.text_sink_0_2);
                _animationUp.addFrame(R.drawable.text_sink_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_OBJ_hot":
                _animationUp.addFrame(R.drawable.text_hot_0_1);
                _animationUp.addFrame(R.drawable.text_hot_0_2);
                _animationUp.addFrame(R.drawable.text_hot_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_OBJ_melt":
                _animationUp.addFrame(R.drawable.text_melt_0_1);
                _animationUp.addFrame(R.drawable.text_melt_0_2);
                _animationUp.addFrame(R.drawable.text_melt_0_3);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "weed":
                _animationUp.addFrame(R.drawable.weed);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_SUB_weed":
                _animationUp.addFrame(R.drawable.text_weed);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
            case "text_OBJ_high":
                _animationUp.addFrame(R.drawable.text_high);
                _animationUp.setDelay(10);
                _animationDown = _animationLeft = _animationRight = _animationUp;
                break;
        }
        result.add(_animationUp);
        result.add(_animationDown);
        result.add(_animationLeft);
        result.add(_animationRight);
        return result;
    }
}
