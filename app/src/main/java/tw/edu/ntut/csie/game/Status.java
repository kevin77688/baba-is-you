package tw.edu.ntut.csie.game;

public class Status {
    protected boolean _setNull;
    protected boolean _checkMoved;
    protected boolean _isShow;
    protected boolean _isPush;
    protected boolean _isStop;
    protected boolean _isYou;
    protected boolean _isWin;
    protected boolean _isSink;
    protected boolean _isDefeat;
    protected boolean _isHot;
    protected boolean _isMelt;

    public Status() {
        setAllStatusNull();
    }

    public void setAllStatusNull() {
        _setNull = false;
        _checkMoved = false;
        _isShow = true;
        _isPush = false;
        _isStop = false;
        _isYou = false;
        _isWin = false;
        _isSink = false;
        _isDefeat = false;
        _isHot = false;
        _isMelt = false;
    }

    public void setAllStatus(Status status) {
        _setNull = status._setNull;
        _checkMoved = status._checkMoved;
        _isShow = status._isShow;
        _isPush = status._isPush;
        _isStop = status._isStop;
        _isYou = status._isYou;
        _isWin = status._isWin;
        _isSink = status._isSink;
        _isDefeat = status._isDefeat;
        _isHot = status._isHot;
        _isMelt = status._isMelt;
    }

    public void setStatus(String status) {
        if (_isShow)
            switch (status) {
                case "push":
                    _isPush = true;
                    break;
                case "stop":
                    _isStop = true;
                    break;
                case "you":
                    _isYou = true;
                    break;
                case "win":
                    _isWin = true;
                    break;
                case "sink":
                    _isSink = true;
                    break;
                case "defeat":
                    _isDefeat = true;
                    break;
                case "hot":
                    _isHot = true;
                    break;
                case "melt":
                    _isMelt = true;
                    break;
                case "disableShow":
                    setAllStatusNull();
                    _isShow = false;
                    break;
                default:
                    throw new RuntimeException("set Status failed !");
            }
    }

    public boolean isPush() {
        return _isPush;
    }

    public boolean isStop() {
        return _isStop;
    }

    public boolean isWin() {
        return _isWin;
    }

    public boolean isYou() {
        return _isYou;
    }

    public boolean isSink() {
        return _isSink;
    }

    public boolean isShow() {
        return _isShow;
    }

    public boolean isDefeat() {
        return _isDefeat;
    }

    public boolean isHot() {
        return _isHot;
    }

    public boolean isMelt() {
        return _isMelt;
    }

    public boolean checkMoved() {
        return _checkMoved;
    }

    public void setMoved() {
        _checkMoved = true;
    }

    public void resetCheckMoved() {
        _checkMoved = false;
    }

    public boolean getNullTag() {
        return _setNull;
    }

    public void setNullTag() {
        _setNull = true;
    }
}
