package tw.edu.ntut.csie.game;

import java.util.ArrayList;

import tw.edu.ntut.csie.game.pattern.command.StatusCommand;
import tw.edu.ntut.csie.game.pattern.observer.StaticObserver;

public class BlockStatus {
    private ArrayList<MovableObject> _allItem;
    private StaticObserver _observer;

    public BlockStatus() {
        _allItem = new ArrayList<>();
        _observer = StaticObserver.getInstance();
    }

    public ArrayList<StatusCommand> add(MovableObject movableObject) {
        _allItem.add(movableObject);
        return blockChanges();
    }


    public void remove(MovableObject movableObject) {
        _allItem.remove(movableObject);
    }

    public boolean checkObjectExist(MovableObject movableObject) {
        return _allItem.contains(movableObject);
    }

    public MovableObject checkStatus(String string) {
        switch (string) {
            case "push":
                for (MovableObject object : _allItem)
                    if (object.isPush() == true || object.isStop())
                        return object;
                break;
            case "stop":
                for (MovableObject object : _allItem)
                    if (object.isStop() == true)
                        return object;
                break;
            case "win":
                for (MovableObject object : _allItem)
                    if (object.isWin())
                        return object;
                break;
            case "you":
                for (MovableObject object : _allItem)
                    if (object.isYou())
                        return object;
                break;
            case "sink":
                for (MovableObject object : _allItem)
                    if (object.isSink())
                        return object;
                break;
            case "hot":
                for (MovableObject object : _allItem)
                    if (object.isHot())
                        return object;
                break;
            case "melt":
                for (MovableObject object : _allItem)
                    if (getObject().isMelt())
                        return object;
                break;
            case "defeat":
                for (MovableObject object : _allItem)
                    if (object.isDefeat())
                        return object;
                break;
            default:
                throw new RuntimeException("Get Status failed !");
        }
        return null;
    }

    public MovableObject getSubject() {
        for (MovableObject object : _allItem)
            if (object.getAnimation().contains("SUB"))
                return object;
        return null;
    }

    public MovableObject getObject() {
        for (MovableObject object : _allItem)
            if (object.getAnimation().contains("OBJ"))
                return object;
        return null;
    }

    private ArrayList<StatusCommand> blockChanges() {
        ArrayList<StatusCommand> returnCommand = new ArrayList<>();
        MovableObject you = null, win = null, push = null, sink = null, defeat = null, hot = null, melt = null;
        for (MovableObject object : _allItem) {
            if (object.isYou())
                you = object;
            if (object.isWin())
                win = object;
            if (object.isPush())
                push = object;
            if (object.isSink())
                sink = object;
            if (object.isDefeat())
                defeat = object;
            if (object.isHot())
                hot = object;
            if (object.isMelt())
                melt = object;
        }
        if (you != null && win != null)
            _observer.setWinStatement(true);
        if (sink != null && you != null) {
            returnCommand.add(new StatusCommand(you, "disableShow"));
            returnCommand.add(new StatusCommand(sink, "disableShow"));
        }
        if (sink != null && push != null) {
            returnCommand.add(new StatusCommand(push, "disableShow"));
            returnCommand.add(new StatusCommand(sink, "disableShow"));
        }
        if (hot != null && melt != null) {
            returnCommand.add(new StatusCommand(melt, "disableShow"));
        }
        if(defeat != null && you != null){
            returnCommand.add(new StatusCommand(you, "defeat"));
            returnCommand.add(new StatusCommand(you, "disableShow"));
        }
        return returnCommand;
    }
}
