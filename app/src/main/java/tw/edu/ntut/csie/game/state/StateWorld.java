package tw.edu.ntut.csie.game.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tw.edu.ntut.csie.game.R;
import tw.edu.ntut.csie.game.World;
import tw.edu.ntut.csie.game.core.MovingBitmap;
import tw.edu.ntut.csie.game.engine.GameEngine;
import tw.edu.ntut.csie.game.extend.BitmapButton;
import tw.edu.ntut.csie.game.extend.ButtonEventHandler;
import tw.edu.ntut.csie.game.gameFrameworkCore.Game;
import tw.edu.ntut.csie.game.gameFrameworkCore.Pointer;
import tw.edu.ntut.csie.game.pattern.singleton.GlobalVariable;

public class StateWorld extends GameState {
    private BitmapButton _level_01, _level_02, _level_03, _level_04,
            _level_05, _level_06;
    private ArrayList<BitmapButton> _allLevel;
    private World _world;
    private MovingBitmap _background;

    public StateWorld(GameEngine engine) {
        super(engine);
    }

    public void initialize(Map<String, Object> data) {
        _level_01 = new BitmapButton(R.drawable.level_1, R.drawable.level_1, 100, 100);
        _level_02 = new BitmapButton(R.drawable.level_2, R.drawable.level_2, 250, 100);
        _level_03 = new BitmapButton(R.drawable.level_3, R.drawable.level_3, 400, 100);
        _level_04 = new BitmapButton(R.drawable.level_4, R.drawable.level_4, 100, 200);
        _level_05 = new BitmapButton(R.drawable.level_5, R.drawable.level_5, 250, 200);
        _level_06 = new BitmapButton(R.drawable.level_6, R.drawable.level_6, 400, 200);
        _allLevel = new ArrayList<BitmapButton>() {{
            add(_level_01);
            add(_level_02);
            add(_level_03);
            add(_level_04);
            add(_level_05);
            add(_level_06);
        }};
        for (int index = 0; index < _allLevel.size(); index++)
            setEventHandler(_allLevel.get(index), index + 1);
        _world = new World(_allLevel);
        _background = new MovingBitmap(R.drawable.maps_bg);
    }

    @Override
    public void move() {
        _world.move();
    }

    @Override
    public void show() {
        _background.show();
        _world.show();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void keyPressed(int keyCode) {

    }

    @Override
    public void keyReleased(int keyCode) {

    }

    @Override
    public boolean pointerPressed(Pointer actionPointer, List<Pointer> pointers) {
        _world.pointerPressed(actionPointer, pointers);
        return true;
    }

    @Override
    public boolean pointerMoved(Pointer actionPointer, List<Pointer> pointers) {
        _world.pointerMoved(actionPointer, pointers);
        return false;
    }

    @Override
    public boolean pointerReleased(Pointer actionPointer, List<Pointer> pointers) {
        _world.pointerReleased(actionPointer, pointers);
        return false;
    }

    @Override
    public void release() {

    }

    @Override
    public void orientationChanged(float pitch, float azimuth, float roll) {

    }

    @Override
    public void accelerationChanged(float dX, float dY, float dZ) {

    }

    private void setEventHandler(BitmapButton button, int levelNumber) {
        button.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                Map<String, Object> setLevel = new HashMap<String, Object>() {{
                    put("level", levelNumber);
                }};
                GlobalVariable.getInstance().effects.play(Game.ENTER_LEVEL_SOUND);
                changeState(Game.RUNNING_STATE, setLevel);
            }
        });
    }
}
