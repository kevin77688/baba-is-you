package tw.edu.ntut.csie.game.pattern.command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class MovableObjectStatusCommand implements ICommand {

    private ArrayList<ICommand> _commands;

    public MovableObjectStatusCommand(ArrayList<ICommand> commands) {
        _commands = new ArrayList<>();
        ArrayList<ICommand> delayCommand = new ArrayList<>();
        for (ICommand command : commands)
            if (command instanceof AnimationCommand)
                _commands.add(command);
            else
                delayCommand.add(command);
        _commands.addAll(delayCommand);
    }

    @Override
    public void Execute() {
        ArrayList<ICommand> executeCommand = new ArrayList<>();
        executeCommand.addAll(_commands);
        Iterator<ICommand> it = executeCommand.iterator();
        while (it.hasNext())
            it.next().Execute();
    }

    @Override
    public void Discard() {
        ArrayList<ICommand> discardCommand = new ArrayList<>();
        discardCommand.addAll(_commands);
        Collections.reverse(discardCommand);
        Iterator<ICommand> it = discardCommand.iterator();
        while (it.hasNext())
            it.next().Discard();
    }
}
