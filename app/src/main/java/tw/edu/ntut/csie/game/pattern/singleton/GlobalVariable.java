package tw.edu.ntut.csie.game.pattern.singleton;

import android.content.Context;

import tw.edu.ntut.csie.game.R;
import tw.edu.ntut.csie.game.core.SoundEffects;
import tw.edu.ntut.csie.game.gameFrameworkCore.Game;

public class GlobalVariable {
    private static GlobalVariable instance = null;
    public Context context;
    public int numberOfButton;
    public SoundEffects effects;

    private GlobalVariable() {
        effects = new SoundEffects();
        effects.addSoundEffect(Game.WIN_SOUND, R.raw.win);
        effects.addSoundEffect(Game.ENTER_LEVEL_SOUND, R.raw.enter_level);
        effects.addSoundEffect(Game.MOVING_SOUND, R.raw.moving);
        effects.addSoundEffect(Game.RESTART_SOUND, R.raw.restart);
    }

    public static GlobalVariable getInstance() {
        if (instance == null)
            instance = new GlobalVariable();
        return instance;
    }
}
