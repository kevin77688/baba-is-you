package tw.edu.ntut.csie.game.pattern.command;

import tw.edu.ntut.csie.game.MovableObject;

public class AnimationCommand implements ICommand {

    MovableObject _currentObject;
    MovableObject _backupObject;
    String _animation;

    public AnimationCommand(MovableObject object, String animation) {
        _currentObject = object;
        _backupObject = new MovableObject(object);
        _animation = animation;
        if (object.getNullTag())
            object.setAllStatusNull();
    }

    @Override
    public void Execute() {
        _currentObject.setAnimation(_animation);
    }

    @Override
    public void Discard() {
        _currentObject.recover(_backupObject);
    }
}
