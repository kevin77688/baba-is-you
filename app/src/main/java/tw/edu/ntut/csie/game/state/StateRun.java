package tw.edu.ntut.csie.game.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import tw.edu.ntut.csie.game.R;
import tw.edu.ntut.csie.game.Stage;
import tw.edu.ntut.csie.game.core.Audio;
import tw.edu.ntut.csie.game.engine.GameEngine;
import tw.edu.ntut.csie.game.extend.BitmapButton;
import tw.edu.ntut.csie.game.extend.ButtonEventHandler;
import tw.edu.ntut.csie.game.gameFrameworkCore.Game;
import tw.edu.ntut.csie.game.gameFrameworkCore.Pointer;
import tw.edu.ntut.csie.game.pattern.observer.StaticObserver;
import tw.edu.ntut.csie.game.pattern.singleton.GlobalVariable;
import tw.edu.ntut.csie.game.pattern.singleton.WorldButtonStatus;

public class StateRun extends GameState implements Observer {

    private Stage _stage;
    private BitmapButton _upArrow, _downArrow, _leftArrow, _rightArrow, _undoButton, _redoButton, _pauseButton;
    private ArrayList<BitmapButton> _allButton;
    private StaticObserver _observer = StaticObserver.getInstance();
    private int _counter;
    private boolean _is_pressed;
    private Pointer _actionPointer;
    private List<Pointer> _pointers;
    private Audio _bgm;
    private Map<String, Object> _data;


    public StateRun(GameEngine engine) {
        super(engine);
    }

    @Override
    public void initialize(Map<String, Object> data) {
        _observer.addObserver(this);
        _data = data;
        _stage = new Stage(data);
        initializeArrow();
        initializeUndoButton();
        initializeRedoButton();
        initializePauseButton();
        _allButton = new ArrayList<BitmapButton>() {{
            add(_upArrow);
            add(_downArrow);
            add(_leftArrow);
            add(_rightArrow);
            add(_undoButton);
            add(_redoButton);
            add(_pauseButton);
        }};
        _bgm = new Audio(R.raw.level);
        _bgm.play();
    }

    private void initializeArrow() {
        initializeUpButton();
        initializeDownButton();
        initializeLeftButton();
        initializeRightButton();
    }


    public void move() {
        _stage.move();
        if (_is_pressed == true){
            _counter++;
            if (_counter >= 3){
                this.pointerReleased(_actionPointer, _pointers);
                this.pointerPressed(_actionPointer, _pointers);
            }
        }
    }

    @Override
    public void show() {
        _stage.show();
        for (BitmapButton button : _allButton)
            button.show();
        _stage.show_pause();
    }

    @Override
    public void release() {
        _stage.release();
    }

    @Override
    public void keyPressed(int keyCode) {
        // TODO Auto-generated method stub
    }

    @Override
    public void keyReleased(int keyCode) {
        // TODO Auto-generated method stub
    }

    @Override
    public void orientationChanged(float pitch, float azimuth, float roll) {
    }

    @Override
    public void accelerationChanged(float dX, float dY, float dZ) {
        // TODO Auto-generated method stub
    }

    @Override
    public boolean pointerPressed(Pointer actionPointer, List<Pointer> pointers) {
        _is_pressed = true;
        _actionPointer = actionPointer;
        _pointers = pointers;
        _counter = 0;
        for (BitmapButton button : _allButton)
            button.pointerPressed(actionPointer, pointers);
        _stage.pointerPressed(actionPointer, pointers);
        return true;
    }

    @Override
    public boolean pointerMoved(Pointer actionPointer, List<Pointer> pointers) {
        for (BitmapButton button : _allButton)
            button.pointerMoved(actionPointer, pointers);
        _stage.pointerMoved(actionPointer, pointers);
        return false;
    }

    @Override
    public boolean pointerReleased(Pointer actionPointer, List<Pointer> pointers) {
        _is_pressed = false;
        for (BitmapButton button : _allButton)
            if (button != null)
                button.pointerReleased(actionPointer, pointers);
        _stage.pointerReleased(actionPointer, pointers);
        return false;
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    private void initializePauseButton() {
        _pauseButton = new BitmapButton(R.drawable.pause, R.drawable.pause, 561,0);
        _pauseButton.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                _stage.pause();
            }
        });
    }

    private void initializeUndoButton() {
        _undoButton = new BitmapButton(R.drawable.undo, R.drawable.undo_pressed, 0, 0);
        _undoButton.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                _stage.undo();
            }
        });
    }

    private void initializeRedoButton() {
        _redoButton = new BitmapButton(R.drawable.redo, R.drawable.redo_pressed, 0, 75);
        _redoButton.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                _stage.redo();
            }
        });
    }

    private void initializeUpButton() {
        _upArrow = new BitmapButton(R.drawable.up, R.drawable.up_pressed, 75, 200);
        _upArrow.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                _stage.up();
            }
        });
    }

    private void initializeDownButton() {
        _downArrow = new BitmapButton(R.drawable.down, R.drawable.down_pressed, 75, 300);
        _downArrow.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                _stage.down();
            }
        });
    }

    private void initializeLeftButton() {
        _leftArrow = new BitmapButton(R.drawable.left, R.drawable.left_pressed, 25, 250);
        _leftArrow.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                _stage.left();
            }
        });
    }

    private void initializeRightButton() {
        _rightArrow = new BitmapButton(R.drawable.right, R.drawable.right_pressed, 125, 250);
        _rightArrow.addButtonEventHandler(new ButtonEventHandler() {
            @Override
            public void perform(BitmapButton button) {
                _stage.right();
            }
        });
    }

    @Override
    public void update(Observable observable, Object o) {
        if (_observer.getWinStatement() == true) {
            WorldButtonStatus.getInstance().setButtonStatus(_stage.getLevel());
            GlobalVariable.getInstance().effects.play(Game.WIN_SOUND);
            _bgm.stop();
            changeState(Game.WORLD_STATE);
        } else if (_observer.getChangeState() == true) {
            _bgm.stop();
            changeState(Game.WORLD_STATE);
        } else if (_observer.getBackToMenu() == true) {
            _bgm.stop();
            changeState(Game.INITIAL_STATE);
        } else if (_observer.getRestart() == true) {
            GlobalVariable.getInstance().effects.play(Game.RESTART_SOUND);
            _bgm.stop();
            this.initialize(_data);
        } else if (_observer.getEaster() == true) {
            _bgm.stop();
            Map<String, Object> setLevel = new HashMap<String, Object>() {{
                put("level", -1);

            }};
            this.initialize(setLevel);
        }
    }
}