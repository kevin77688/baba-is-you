package tw.edu.ntut.csie.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tw.edu.ntut.csie.game.extend.BitmapButton;
import tw.edu.ntut.csie.game.gameFrameworkCore.Pointer;
import tw.edu.ntut.csie.game.pattern.singleton.GlobalVariable;
import tw.edu.ntut.csie.game.pattern.singleton.WorldButtonStatus;

public class World {
    ArrayList<BitmapButton> _allButton;
    HashMap<Integer, Boolean> _buttonStatus;

    public World(ArrayList<BitmapButton> allButton) {
        _allButton = allButton;
        GlobalVariable.getInstance().numberOfButton = _allButton.size();
        _buttonStatus = WorldButtonStatus.getInstance().getButtonStatus();
        for (BitmapButton button : _allButton)
            button.setVisible(false);
        _allButton.get(0).setVisible(true);
        for (int index = 1; index < _allButton.size(); index++)
            if (_buttonStatus.get(index) == true)
                _allButton.get(index).setVisible(true);
    }

    public void move() {
        for (BitmapButton button : _allButton)
            button.move();
    }

    public void show() {
        for (BitmapButton button : _allButton)
            button.show();
    }

    public void pointerPressed(Pointer actionPointer, List<Pointer> pointers) {
        for (BitmapButton button : _allButton)
            button.pointerPressed(actionPointer, pointers);
    }

    public void pointerMoved(Pointer actionPointer, List<Pointer> pointers) {
        for (BitmapButton button : _allButton)
            button.pointerMoved(actionPointer, pointers);
    }

    public void pointerReleased(Pointer actionPointer, List<Pointer> pointers) {
        for (BitmapButton button : _allButton)
            if (button != null)
                button.pointerReleased(actionPointer, pointers);
    }

}
