package tw.edu.ntut.csie.game.pattern.singleton;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public class WorldButtonStatus {
    private static WorldButtonStatus instance = null;
    private HashMap<Integer, Boolean> _buttonStatus;
    private int _numberOfButton;

    @SuppressWarnings("unchecked")
    private WorldButtonStatus() {
        _numberOfButton = GlobalVariable.getInstance().numberOfButton;
        try {
            File file = new File(GlobalVariable.getInstance().context.getFilesDir(), "level.dat");
            if (!file.exists()) {
                _buttonStatus = new HashMap<>();
                for (int index = 1; index <= _numberOfButton; index++)
                    _buttonStatus.put(index, false);
                FileOutputStream outputStream = new FileOutputStream(file);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

                objectOutputStream.writeObject(_buttonStatus);
                objectOutputStream.close();
            } else {
                ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
                _buttonStatus = ((HashMap<Integer, Boolean>) objectInputStream.readObject());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static WorldButtonStatus getInstance() {
        if (instance == null)
            instance = new WorldButtonStatus();
        return instance;
    }

    public HashMap<Integer, Boolean> getButtonStatus() {
        return _buttonStatus;
    }

    public void setButtonStatus(int level) {
        _buttonStatus.put(level, true);
        saveFile();
    }

    public void resetButtonStatus() {
        _buttonStatus = new HashMap<>();
        for (int index = 1; index <= _numberOfButton; index++)
            _buttonStatus.put(index, false);
        saveFile();
    }

    private void saveFile() {
        try {
            File file = new File(GlobalVariable.getInstance().context.getFilesDir(), "level.dat");
            FileOutputStream outputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

            objectOutputStream.writeObject(_buttonStatus);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
