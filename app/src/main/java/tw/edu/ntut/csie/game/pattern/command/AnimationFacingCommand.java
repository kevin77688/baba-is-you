package tw.edu.ntut.csie.game.pattern.command;

import tw.edu.ntut.csie.game.MovableObject;

public class AnimationFacingCommand implements ICommand {

    private MovableObject _backupObject;
    private MovableObject _currentObject;
    private int _facing;

    public AnimationFacingCommand(MovableObject currentObject, int facing) {
        _backupObject = new MovableObject(currentObject);
        _currentObject = currentObject;
        _facing = facing;
    }

    @Override
    public void Execute() {
        _currentObject.setAnimationFacing(_facing);
    }

    @Override
    public void Discard() {
        _currentObject.recover(_backupObject);
    }
}
