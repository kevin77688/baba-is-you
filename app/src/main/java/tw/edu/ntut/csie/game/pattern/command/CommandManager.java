package tw.edu.ntut.csie.game.pattern.command;

import java.util.Stack;

public class CommandManager {
    Stack<ICommand> _undo, _redo;

    public CommandManager() {
        _undo = new Stack<>();
        _redo = new Stack<>();
    }

    public void Execute(ICommand command) {
        command.Execute();
        _undo.push(command);
        _redo.clear();
    }

    public void Undo() {
        if (_undo.size() < 1)
            System.out.println("Undo stack is Empty");
        else {
            ICommand command = _undo.pop();
            _redo.push(command);
            command.Discard();
        }

    }

    public void Redo() {
        if (_redo.size() < 1)
            System.out.println("Redo stack is empty");
        else {
            ICommand command = _redo.pop();
            _undo.push(command);
            command.Execute();
        }
    }
}
