package tw.edu.ntut.csie.game.pattern.command;

public interface ICommand {
    void Execute();

    void Discard();
}
