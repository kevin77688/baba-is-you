package tw.edu.ntut.csie.game.pattern.builder;

import java.util.ArrayList;

import tw.edu.ntut.csie.game.Location;
import tw.edu.ntut.csie.game.MovableObject;

public class BuildObject {

    private ArrayList<MovableObject> _blockList;
    private ArrayList<Location> _blockLocation;
    private Location _boundaryLocation;

    public BuildObject(){
        _blockList = new ArrayList<>();
        _blockLocation = new ArrayList<>();
    }

    public void buildObject(String category, int x, int y){
        if (category.equals("boundary"))
            _boundaryLocation = new Location(x, y);
        else{
            _blockList.add(new MovableObject(category));
            _blockLocation.add(new Location(x, y));
        }
    }

    public ArrayList<MovableObject> getBlockList(){
        ArrayList<MovableObject> result = new ArrayList<>();
        result.addAll(_blockList);
        return result;
    }

    public ArrayList<Location> getBlockLocation(){
        ArrayList<Location> result = new ArrayList<>();
        result.addAll(_blockLocation);
        return result;
    }

    public Location getBoundaryLocation(){
        if (_boundaryLocation != null)
            return new Location(_boundaryLocation);
        throw new RuntimeException("boundary location in buildObject is Null !");
    }
}
