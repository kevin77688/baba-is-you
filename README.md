# Design pattern project :  Baba is You
## Team Member
* 106820005 汪謙益
* 106820007 段凱文

## What is Baba is You
 **Baba is you** 是一個邏輯解謎遊戲，透過修改畫面上的文字方塊去建構遊戲中的邏輯，進而影響關卡的運作機制。

 - Example：畫面上如果有由三個文字方塊構成的句子 " Wall is stop "，則玩加在操作主角時便無法移動。如果玩家改變控制方塊的排列方式時，，則碰到牆壁會停止的這個規則便不成立。而 Baba is You 便是圍繞著這個機制去設計的關卡解謎遊戲。
![](data/tutorial.webm)


## Objects in Baba is you
 - Moviable object: 畫面上可被移動的方塊。包含實體物品(如 Baba、牆壁、石頭 ... etc.)以及構成邏輯的文字方塊。
 - Block status: 管理畫面上地圖某一格現在包含的內容物
 - CreateLevel: 透過讀取 Xml 檔創造各個關卡應有的物件
 - World: 關卡切換的 State
	

## Design Patterns

### Force Diagram
<img src="data/Force.png" width="800">

### Class Diagram
- Command Pattern <br>
    <img src="data/Command.png" width="800">
- Observer Pattern <br>
    <img src="data/Observer.png" width="800">
- State Pattern <br>
    <img src="data/State.png" width="800">
- Builder Pattern <br>
    <img src="data/Builder.png" width="800">
- Singleton Pattern <br>
    <img src="data/Singleton.png" width="800">


### Detail

 - **Command Pattern :** 
    -  **Motivation ：** 
    Baba is You 是一個邏輯解謎遊戲，有時候因為移動錯誤而早成卡關。為了避免這種情況，我們需要設計 **Undo** 與 **Redo** 按鈕。但是 Undo 與 Redo 牽涉到很多動作，例如: 按向右的方向鍵可能會導致 ：
        > 1.  玩家操控的角色的動畫會改變 
        > 2.  玩家操控角色位置會改變
        > 3.  玩家可能會動到畫面上的邏輯方塊導致某些方塊的狀態被改變

        所以我們需要有系統地去記錄目前為止玩家所操作的行為

    -   **Implementation ：**
        ![](data/undoredo.mp4)
        套用Command Pattern後，我們把上面列出來的各種 Request 轉變成ˊ物件，方便管理。
        首先我們會有 ICommand 作為 Interface，底下會有 Execute() 與 Discard() 兩種 method。其中
         - Execute() 會去執行User控制所生成的指令，包含各種物件的屬性設置
         - Discard() 會去做完全相反的動作，將已經發生的指令去還原
        而上列的命令會由以下來管理
        > 1. animationCommand( ) -> 玩家移動時而造成的動畫改變
        > 2. moveCommand( ) -> 玩家移動時而改變物件的位置
        > 3. statusCommand( ) -> 玩家改變方塊排列而改變遊戲的邏輯改變

        **Sample Code** <br>
        <img src="data/CommandCode.png" width="450">
        
    -   **Consequences ：**
        - 會將原本執行該指令的實作從他的責任區域抽出，改在 Command 裡實作，要執行實呼叫該 Command
        - 每多一種新的動作會需要新增會一個新的 Object 去時做 Command Pattern
        - Command 與一般 Object 行為無異，可以任意擴充
        - 套用 Composite Pattern，可以將多個 Command 組成一個新的 Composite Command 以方便管理
        - 新增 Command時很方便，因為不會需要修改既有的 Object

- **Observer Pattern：**
    - **Motivation：**
    Baba is You的遊戲機制能夠讓玩家透過改變畫面上的文字語句來改變遊戲裡面的規則，其中"flag is win"表示玩家碰到旗子的時候該關便結束，切換到關卡選擇畫面，這個動作牽涉到遊戲會從state run切換到 state world。我們需要一個管道通知state run什麼時候需要切換state

    - **Implemantation：**
    我們利用Observer觀察"is_win"這個boolean值,每當這個值從false變成true時(publisher)，*Observer* 會通知 State_run，State_run 便會執行 update() 這個function去切換 state(subscriber)。

    **Sample Code** <br>
        <img src="data/ObserverCode.png" width="450">

    -   **Consequences ：**
    可以廣式的通知任何對這項改變有興趣的subscriber，改變此參數的動作不需要知道有誰想知道由於Observer對於通知其他人之後會有怎樣的行為是完全無知的，所以可能會造成預期外的行為
    
 - **State Pattern：**
    - **Motivation：**
    遊戲當中每個場景對於不同的關卡會有不同的回應，希望能夠將每個場景分別獨立開來，而使用者的動作仍然相同 ( 皆是於螢幕上作互動 ) ，因此這裡使用 State Pattern 做隔離
    - **Implemantation：**
    在遊戲當中的不同階段可以呼叫不同的 Class 去做回應，在此我們對於遊戲分為四種不同的階段
 ![](data/changestate.mp4)
        > 1. StateReady( )  ：遊戲剛開始時的開始畫面
        > 2. StateWorld( )  ：在進入遊戲之後，選擇關卡畫面，包含紀錄時間，過關數量
        > 3. StateRun( )    ：選擇完關卡之後，進入遊戲的本體，並從 StateWorld ( )去獲得目前選擇的關卡，並執行之
        > 4. StateOver( )   ：當所有的關卡都已經結束時，也就是遊戲整體結束， StateWorld ( ) 將呼叫本 State，慶祝遊玩結束~
        **Sample Code** <br>
        <img src="data/StateCode.png" width="450">

    -   **Consequences ：**
        -   可以將每個不同場景去做不同的反應，並且每個場景互不干擾，可以有效的減少錯誤
        -   當有 Class 創建於這個 State 當中的時候，只要切換 State 就不會受到影響，避免在其他 State 也用到同樣的 Class 的時候，不會受到上個狀態影響
        -   當配合 Singleton Pattern 的時候，可以確保每次 Runtime 的時候只會有一個 State 在活動，而切換時也只會需要創建一次，避免資源的浪費，增加程式的效率
   
- **Builder Pattern：**
    -   **Motivation ：** 
        當我們在建立一個關卡內容時，最初是寫死在程式碼裡面並且把物件建立出來，但是這樣的做法，並不能自由的擴充關卡，每當要創立新的關卡時，就會需要一關就包成一個 Class ，以程式擴充性來看，並不是一個好的方法，希望能夠將關卡資訊打包成 dat 檔，當要建立場景時可以直接讀取外部檔案。
    -   **Implementation ：**
        *   目前的關卡資料包含
        1.  地圖邊界大小 X * Y
        2.  每個關卡中可被互動的物件 ( *Object* ）資料都是存在 Xml 檔案裡面，透過讀取資訊可以生成關卡，並且使用 *CreateLevel* 先把資料切割整理之後 ， 在由 *BuildObject* 來建立物件  

        **Sample Xml** <br>
        <img src="data/xmlSample.png" width="450">

        **Sample Code** <br>
        <img src="data/BuilderCode.png" width="450">

    -   **Consequences ：**
        -   當我們想要把物件設定額外屬性的時候，可以直接利用 *Builder* 來更改，比較容易分析語監控物件創立的過程
        -   能夠以抽象界面將資料型態創造出實體物件
        -   將負責表達資料格式的部分與製造物件的部分隔離開來 

## Evolution

| Pattern      | Before                                                 | After                                                        |
| ------------ | ------------------------------------------------------ | -----------------------------------------------------------  |
| Command      | 所有的命令都是直接執行，<br>無法追蹤                   | 所有的動作都可以追蹤，<br>方便Trace問題， 還可以Undo Command |  
| Observer     | 所有的資料都要靠參數傳輸，<br>或是時時去追蹤           | 只要變數有更新，就會被通知，<br> 不需要額外花資源去檢查      |
| State        | 無法確保環境的獨立性，<br>不容易擴充                   | 可以自由擴充，不會受到影響                                   |
| Builder      | 要生成的物件要寫死，<br>需要有新功能的時候要額外添加   | 想要新增物件的時候可以直接更改，<br> 容易擴充                |